A user browses to https://gitlab.com/gitlab-org/gitlab-ce in their browser. Please describe in as much detail as you
think is appropriate the lifecycle of this request and what happens in the browser, over the network, on GitLab
servers, and in the GitLab Rails application before the request completes.

# TL;DR Version #

(NOTE- this is the same answer submitted in the application via the web form.)

When a user types a URL into their browser and hits enter, the browser looks up the IP address of the domain by
contacting a domain name server.  Once it knows this IP address, it initiates a connection to the server at that
address and sends an HTTP request, which consists of a verb (i.e. GET or POST), the path to the resource it wants (in
this case '/gitlab-org/gitlab-ce'), some request headers, and potentially some query string parameters.  This request
is labeled with the destination's IP address, port number, and some other identifying information so that it gets
routed to the correct place.  Finally, the request is split up into pieces called frames, and sent piece by piece over
the wire in the form of 1s and 0s to its destination.

When the frames reach their destination, the server re-assembles them into a single request and routes it to the
application listening on the port specified in the request.  In GitLab's case, when the Rails server receives this
request, it uses the routes.rb file to route the request to the correct controller and action (here, it's the
ProjectsController's 'show' action).  The Rails router deconstructs the path to fetch the namespace ID ('gitlab-org')
and project ID ('gitlab-ce'), and the controller action uses these IDs to retrieve the correct project from the
database.  Once the right project is identified, the controller redirects the user to another URL if the project is in
the middle of an import.  It also sets an alert message if the project is pending deletion.  Finally, it returns the
project as a response, in the format requested by the client.  If the user's client is a regular web browser, the
response body takes the form of HTML.  If the client was an RSS feed client, it takes the form of an Atom RSS feed.
In addition to this response body, the Rails web server includes a response code (i.e. 200 for a successful response)
and some response headers as well, telling the client things like the Content-Type and the Content-Length.

The web server packages the response and splits it into frames as well, just as the client did with the request.
After the response makes its way back to the client's IP address, the user's OS re-packages the response from frames
into a response object and hands the response to the web browser.  The browser turns the body of the response into an
HTML document and renders it on the page, layering the CSS styling on top and executing any included Javascript code.

# Detailed Version (Work In Progress) #

## User enters characters into browser URL bar ##
As the user types the characters which make up the full address, most modern browsers will interpret each keystroke as
a new signal and use that signal to update the contents displayed in a dropdown menu below the address bar input field.
Those contents are made up of suggestions for which URL the browser thinks the user intends to enter, and are
populated by an algorithm that takes into account browsing history, cookies, bookmarks, etc.

## User hits the 'Enter' key ##

Once the user is finished typing and hits the Enter key, the browser breaks down the input into two pieces: the
protocol (i.e. ‘http’) and the URL (i.e. ‘www.gitlab.com/gitlab-org/gitlab-ce’).  If not protocol is found, or the
“URL” is not formatted as a typical URL (i.e. it more closely resembles a word or phrase), the browser may interpret
the input as a search string, and send the contents to its default search engine (i.e. Google for the Chrome browser).
To perform this search, it makes a network request similar to the one it would have made if it had interpreted the
browser input as a URL (this process is described below).

## If the URL is formatted correctly ##

If the URL is formatted correctly, the browser will check that URL against its list of URLs that have previously asked
to be contacted via HTTPS only.  If the URL is found, the browser will use the HTTPS protocol regardless of which
protocol the user specified in their input.

## Browser checks its own cache ##

Next, the browser checks the user-specified URL against a list of URLs in the browser’s cache.  If not found there, it
then asks the OS to check for the address.

## Browser checks the 'hosts' file ##

The OS checks a file on the machine called “/etc/hosts” on a Mac (or something equivalent on a PC) to see if there are
settings local to that client machine which dictate which IP address the URL should map to (i.e. “localhost should
resolve to 0.0.0.0”).

## Browser checks the machine's DNS cache ##

If the URL is not found in the “hosts” file, the OS checks the computer’s own cache (called the domain name server
cache or DNS cache).  This cache is similar to the browser’s own cache in terms of how it stores, retrieves, and
expires cache entries, and also in terms of how it deals with cache hits and misses.

## OS initiates a network call to the remote DNS server(s) ##

### Contacting the recursive DNS servers ###

If the URL isn’t found in the DNS cache, the OS will have to make a network call to an actual DNS (not a local cache)
to look up the IP.  The job of the DNS is to map human-friendly URLs to computer-friendly IP addresses.  The OS gets
the DNS’s IP address from either the router or the default gateway IP, i.e. the Internet Service Provider.  Once it has
this IP, a request is made to the DNS to map the URL to its IP address.  If this DNS doesn’t have the relevant IP in
its cache (i.e. if no other user similar to us has typed in the same address at some prior point), it sends a request
to other DNS servers, until either

1. it successfully finds the URL or
2. it determines the URL isn’t known to any DNS.

### Contacting the non-recursive DNS servers ###

These other DNS servers include root DNS servers (which examine the top-level domain, such as .com or .org, and
determine where to route the request from there), top-level DNS servers (whose job it is to direct us to the right
authoritative DNS server for that TLD), and authoritative DNS servers (whose job it is to actually perform the mapping
from the URL to the IP address).  This is where our search for the IP address ends.

### IP address makes its way back to the client ###

The IP address makes its way from the authoritative DNS server back to the top-level DNS server, then back to the root
DNS server, then the recursive DNS server, then to the browser.  On this trip back to the browser, the recursive DNS
server, our local machine, and the browser all cache the result.  This means that the next time this URL is requested,
the request doesn’t have to travel as far and the result can be returned much faster.

#### A brief aside ####

Think of the above process as being similar to a person who wants to visit another person’s house.  You can’t just tell
the driver “take me to my friend’s house” if the driver doesn’t know where they live.  You need to give the driver an
address.  An IP address is like a street address for a computer.

Once the browser has the IP address, it knows how to find the other computer (in this case, the server which hosts the
web application we are trying to access, aka gitlab.com).

## TCP Handshake ##

Once the browser has the IP address, it knows how to find the other computer (in this case, the server which hosts the
web application we are trying to access, aka gitlab.com).  Now it needs to initiate a conversation.  Think of this as
being similar to a phone conversation between 2 people (the metaphor will make more sense if you visualize phone calls
before the advent of caller ID).  The first person (the client) dials the number and waits for a response.  The 2nd
person (the server) answers and says "Hello?", implicitly acknowledging they have received the call. The 3rd person
responds to person #2, acknowledging person #2's acknowledgement.  From then on, the conversation can begin.

The browser initiates a TCP connection with the server at that IP address, performing a process known as a “3-way
handshake”.  First the client sends a single data packet called a SYN packet to the server.  This represents the
client asking the server if it is open for new connections.  If the server has open ports, it acknowledges the SYN
packet with an SYN/ACK packet.  Once the client receives the SYN/ACK packet, it acknowledges this with an ACK packet.
From that point on, a TCP connection is established and data can be transmitted.

## HTTP Request ##

Next, the browser sends an HTTP request to the IP address and port specified in the URL (if no port is given, it
defaults to 80 for the HTTP protocol and 443 for the HTTPS protocol).  Think of HTTP as the language used by the
browser and server to talk to each other.  A metaphor would be that, when you go to a store to buy something, you and
the store clerk will likely communicate with each other in English.

### HTTP Verbs ###

In this case, since the user is just requesting a resource and is not sending any data, the browser will issue a GET
request.  If the user were submitting a form (i.e. entering their username and password into a login page or
registering as a new user), this would instead be an HTTP POST request.  This request contains certain metadata, such
as any cookie information for the URL that the browser has stored, what type of content the browser can accept from
the response (i.e. HTML, JSON, images, etc.), and the type of client with which the user sent the request (for
example, which web browser).

### Request Format ###

The HTTP request itself is just a block of text that looks like this:

```
POST /gitlab-org/gitlab-ce HTTP/1.1
Host: www.gitlab.com
Accept-Language: en
Content-Length: 64
Content-Type: application/x-www-form-urlencoded

name=Joe%20User&request=Send%20me%20one%20of%20your%20catalogue
```

There are certain conventions in place to ensure that web servers can break down this block of text into the 3
different sections that make up an HTTP request:

-The first section is always the first line from the request, and is delimited by a carriage return.
-The second section of the request represents the headers of the request (i.e. the host that should receive the
request, the languages accepted, etc.), and is constructed from all lines starting from the second line and ending at
the first empty line.
-The last (and optional) section of the request represents any data payload (in the form of key-value pairs) which is
submitted as part of the request (such as you might send when submitting a form).  For example, in the request above,
the submitted key-value pairs were "name=Joe User" and "request=Send me one of your catalogue".  These key value pairs
are formed using any data after the above empty line.

#### The TCP Model (aka translating the request text into 1s and 0s) ####

After the request is turned into a block of text which obeys the above conventions, the OS wraps it in multiple
layers, like an onion.  Each layer contains information needed to help speed the request from its origin to its
destination.  This layering process is known as the TCP model.  Actually, the above layers of creating the request,
formatting it as text, and starting the 3-way TCP handshake represent the first layer of this model, called the
Application Layer.  The next layer is the Transport Layer, where the client decides to use a certain protocol (i.e.
TCP, UDP, etc.), breaks the single request into pieces called segments, and adds the port numbers for both the client
and server as the next layer in our onion.  Next is the Network Layer, where the IP addresses for both client and
server are added.  After that comes the Data Layer, where the MAC address of both the client's router and the server
is added.  Finally, the Physical Layer is where the data is sent out over the physical cables that make up the
backbone of the Internet.

As briefly mentioned earlier, the request is not sent all in one piece; rather, it is broken up into segments by the
transport layer, and it's each of these segments that get wrapped like an onion.  This process of breaking up the
request into pieces is done so that it's easier for the data to make its way across the Internet, even in the face of
malfunctioning network nodes.  If some of the data packets are dropped and don't reach their destination, they can be
sent again without affecting the other, successfully-sent data packets.  Another benefit is that breaking up data into
packets allows the web server to potentially handle multiple requests from multiple clients at once.  If the web
server was required to process a single request in its entirety, it would have to finish dealing with that one request
before moving onto the next request, potentially slowing down the application significantly.

## Web Server ##

When the request reaches its destination, it is decapsulated layer by layer, in the reverse order from which it was
originally encapsulated.  The request is sent to the router which owns the receiver's MAC address (specified on the
Data Layer).  That router forwards it on to the IP address from the Network Layer.  The computer at that IP address
forwards the request to the receiver's port number (specified in the Transport Layer).  The web server which is
listening at that port number is in charge of taking the re-assembled request text, and turning it into something
which its application can meaningfully use.   Some common examples in Rails are Puma, Thin, and Unicorn.

The web server may optionally be configured to perform one or more "middleware" steps before handing the request to
the web application.  There are many kinds of steps that can be performed here, such as inspecting the IP address of
the client to make sure it's trusted (or not distrusted), formatting the payload of the request to make sure it takes
the shape the web application expects (i.e. when parsing JSON a certain way), or ensuring the client has sent the
correct header(s) along with the request (i.e. ensuring the presence of JWT authentication tokens).

In the case of Gitlab.com, some of the middleware used includes blocking clients that are attempting to throttle the
web server with too many requests, performing a basic health check (only checks that the web server is handling
requests; doesn't check the database status), modifying the request object to allow easy user authentication
throughout the application, setting different rules for cross-origin requests depending on the request's origin, etc.

## How Rails Apps Handle HTTP Requests ##

Ruby On Rails conventions specify that controllers should be in charge of handling requests and returning responses.
But any real-world Rails app has many different controllers, and the app needs to know which controller to route the
request to.  This is the job of the "config/routes.rb" file.  GitLab's "config/routes.rb" file contains many advanced
modifications, but the short explanation is that it uses a helper method called "draw" to import other route-related
files, where each file contains the routes for a single resource (such as the "project" resource, which is the one
we're concerned with for the purposes of our URL).  The file "config/routes/project.rb" contains the routes we care
about.  This file tells the Rails app that a request for "https://gitlab.com/gitlab-org/gitlab-ce" should be routed to
the ProjectsController's "show" action.  Specifically, the line that begins with "resources(:projects,..." tells the
Rails app that it should make the following 4 mappings of URLs to controller actions:

1) GET request to /*namespace_id/:id/edit => ProjectsController#edit
2) GET request to /*namespace_id/:id => ProjectsController#show
3) PATCH or PUT request to /*namespace_id/:id => ProjectsController#update
4) DELETE request to /*namespace_id/:id => ProjectsController#destroy

Since our request is a GET request to "https://gitlab.com/gitlab-org/gitlab-ce", it maps to ProjectsController#show.

## The ProjectsController ##

Rails instantiates a new instance of the ProjectsController class, and calls its "show" method.  This method, like all
controller actions, is responsible for coordinating the actions of the Model and View portions of the MVC pattern with
respect to showing individual projects (such as the "gitlab-ce" project, belonging to the namespace "gitlab-org").
Every action executes its own logic, depending on the business requirements of the app.  In the case of this action,
if the project in question is currently being imported, the controller redirects the browser to "https://gitlab.
com/gitlab-org/gitlab-ce/import".  If the project is pending deletion, the controller prepares an alert message for
the user to let them know this fact.  Otherwise, one of two things happens:

1) If the user's client expects an HTML response, the Project model's "show" view template is rendered.  Any and all
instance variables declared in the controller's "show" action are passed to the HAML template before Rails compiles
this HAML into HTML, and delivers it back to the browser (described below).
2) If the user's client expects an Atom web feed response, the "gitlab-ce" project is rendered as XML and sent as an
Atom feed to the client.

## The Web App Sends The Response ##

The Rails app constructs a response object, including the HTTP response code (i.e. 200 for a successful response),
instructions telling the client how to cache the response, the type of content sent (i.e. "text/html"), etc.  The app
then passes this response to the web server, which constructs a response that uses similar conventions to those used
when constructing the initial request:

```
HTTP/1.1 200 OK
Date: Sat, 09 Oct 2010 14:28:02 GMT
Server: Apache
Last-Modified: Tue, 01 Dec 2009 20:18:22 GMT
ETag: "51142bc1-7449-479b075b2891b"
Accept-Ranges: bytes
Content-Length: 29769
Content-Type: text/html

<!DOCTYPE html......</html>
```

-The first line is the 'status line', and contains the HTTP version used, the HTTP status code (in this case, 200) and
the human-friendly status message corresponding to that code.
-The next lines from line 2 to the first empty line are the response headers.  In the case of the above response, the
headers include the date of the response, the web server used, the ETag (used for caching the response, to reduce the
need for future requests), and the content type being sent back to the client (in this case, regular HTML).
-Any content after the empty line represents the data returned in the response (in this case, the HTML that the
browser should render).

The web server encodes the response in a form that can be transmitted over the wire and then sends the response back
to the client, using the IP address on the request headers as the destination address.

## The Browser Receives The Response ##

The OS of the machine that the client is running on receives the response, performs a similar process to that which
the web server performed with the request, and then passes it on to the browser.  When the browser sees that the
"Content-Type" of the response is "text/html", it renders the bare-bones HTML contained in the response, followed by
any images, video, CSS/Javascript files etc, that need to be fetched in separate requests. Once it finishes rendering
the HTML, it will then make similar HTTP requests to fetch these other resources.  Once it has no more resources to
render, the request/response cycle is complete.

### What is meant by 'Rendering' ###

Among other components, the browser application contains a rendering engine, whose job it is to take the re-packaged
response body from the OS, and construct a document which the browser can then show to the user.  It constructs a
data structure called a parse tree, which contains the HTML code and which the browser knows how to render into something which is relatively user-friendly.  The algorithm to do this is surprisingly complicated, because browsers must be able to correctly interpret certain common cases of invalid HTML.  Additionally, the code being parsed can change *while it's being parsed*, such as when Javascript code is executed which adds/removes elements from the page.

Separately, the browser also contains a Javascript engine, whose job is to take an Javascript-specific content (either
included inline (i.e. directly in the HTML body) or imported from external links) and execute that Javascript code in
the browser.
